<?php

namespace cristian\Http\Controllers;
use cristian\Project;
use Illuminate\Http\Request;
class ProjectsController extends Controller
{
    //
public function index(){
//$projects=\cristian\Project::all();
$projects=Project::all();
//return $projects;
	return view ('projects.index', compact('projects'));
}
public function show(Project $project){
 // $projects=Project::findOrFail($id); lo puedo trabajar mediante el id del registro
  //return $project; retorno la respuesta en formato jason
 return view('projects.show',compact('project'));
}
public function create(){
    return view('projects.create');
 }
 public function store(){


    request()->validate([
   'title'=>['required','min:3'],
   'description'=>['required','max:2']
]);
Project::create(request(['title','description']));
//Project::create($validated);
return redirect('/projects');
//  Project::create(request()->all());
// return'done';
// dd(request()->all());
// dd( [
//   'title'=>request('title'),
//   'description'=>request('description')
// ]);
//  Project::create([
//      'title'=>request('title'),
//      'description'=>request('description')
//  ]);
  // $project= new Project();
  // $project->title=request('title');
  // $project->description=request('description');
  // $project->save();
//  return redirect('/projects');
 }
 public function edit($id){
    // example.com/projects/1/edit
    $project=Project::find($id);
    return view('projects.edit',compact('project'));
  }
public function update(Project $project){
        // $projects=Project::find($id);
         $project->update(request(['title','description']));
          // $project->title= request('title');
          // $project->description=request('description');
          // $project->save();
          dd(request()->all());
          return redirect('/projects');     
      //dd('hello world');
     }
public function destroy(Project $project){
//  Project::find($id)->delete();
     $project->delete();

 return redirect('/projects');



//dd('delete'.$id);

  }
}
