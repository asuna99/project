<?php
Route::get('/', function () {
    return view('welcome');
});
/*
GET/project(index)
GET/projects/create(create)
GET/projects/1(show)
POST/projects(store)
GET/projects/1/(edit)
...PATCH/projects/1(update)
...DELETE/projects/1(delete)

*/
Route::resource('projects', 'ProjectsController');
Route::patch('/tasks/{task}','ProjectTasksController@update');
Route::post('/completed-taks/{taks}','CompletedTaksController@store');
Route::delete('/completed-taks/{taks}','CompletedTaksController@destroy');




// // Route::get('/projects','ProjectsController@index');
// // Route::get('/projects/create','ProjectsController@create');
// // Route::get('/projects/{project}','ProjectsController@show');
// // Route::post('/projects','ProjectsController@store');
// Route::get('/projects/{project}/edit','ProjectsController@edit');
// // Route::patch('/projects/{project}','ProjectsController@patch');
// // Route::delete('/projects/{project}','ProjectsController@destroy');
