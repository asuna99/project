@extends('layaout')
@section('content')
  <div class="card" style="width: 18rem;">
    <div class="card-body">
      <h5 class="card-title">{{$project->title}}</h5>
      <p class="card-text">{{$project->description}}</p>
      <a href="/projects/{{$project->id}}/edit" class="btn btn-primary">Go somewhere</a>
    </div>
  </div>  
<div class="box">
   @foreach($project->taks as $task)
  <div>
  <!-- PATCH/projects/id/tasks/id
  PATCH/taks/id -->
      <form method="POST" action="/completed-taks/{{$task->id}}">
     
       @if($task->completed)
       @method('DELETE')
       @endif


      @csrf
       <label  class="checkbox {{$task->completed ? 'is-completed':''}}" for="completed">
        <input type="checkbox" name="completed" onChange="this.form.submit()"{{$task->completed ? 'checked':''}}>
        {{$task->description}}
       </label>
      </form>
  </div>
   @endforeach
</div>


<form  method="post" action="/projects/{{$project->id}}/tasks"  class="box">
  @csrf
  <div class="field">
    <label for="description" class="label"> new task</label>
      <div class="control">
           <input type="text" class="input" name="description" placeholder="new task">
       </div>
    </div>
  <div class="field">
      <div class="control">
      <button type="submit" class="button btn btn-primary">Add Task</button>
      </div>
  </div>
    @include('errors')
</form>
@endsection