@extends('layaout')

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/public/css/style.css" />
<link rel="stylesheet" href="css/master.css?n=1">
<link rel="stylesheet" href="{{ asset('css/style.css') }}" />

@section('content')
<form method="POST" action="/projects">
    
@csrf
<div  style="margin-top:20px; " class="container">
<div id="tabla">
    <div  style="margin-top:25px;" class="form-group style">
      <label   class="form-control" for="exampleInputEmail1">title</label>
      <input  type="text" class="input{{$errors->has('title') ? 'is-danger' : '' }}" name="title" type="text" placeholder="ingrese titulo" value="{{old('title')}}">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">descripcion</label>
      <input  class="form-control " name="description"type="textarea" placeholder="descripcion">
    </div>
    <div>
    </div>
    <button type="submit" class="btn btn-primary ">enviar</button>
    @include('errors')
</form>
@endsection
