@extends('layaout')
@section('content')
    <h1 class="title">edit project</h1> 
<form method="post" action="/projects/{{$project->id}}">
{{method_field('PATCH')}}
{{csrf_field()}}
    <div class="field">
        <label class=label for="title">title</label>
        <div class="control">
            <input type="text" name="title" class="input" placeholder="title" value="{{$project->title}}">
        </div>
    </div>
    <div class="field">
        <label class=label for=description>description</label>
        <div class="control">
          <textarea name="description" class="textarea">{{$project->description}}</textarea>
        </div>
    </div>
    <div class="field">
        <div class="control">
            <button type="submit" class="btn btn-warning">update project</button>
        </div>
    </div>
  </form>  
<form method="post" action="/projects/{{$project->id}}">
    @method('delete')
    @csrf
 <div class="field">
        <div class="control">
        
            <button type="submit" class="btn btn-danger">delete project</button>
        </div>
    </div>
</form>
@endsection